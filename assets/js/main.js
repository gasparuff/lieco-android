'use strict';

var inWeb = false;

$(document).ready(function() { 
    if(inWeb) {
        if(bowser.android === true) {
            $('#androidhinweis').css('display','inline');
            $('body').css('padding-top','0%');
        }
        else if ( bowser.ios === true ) {
            $('#ioshinweis').css('display','inline');
            $('body').css('padding-top','0%');
        }   
    }

  $('#owl-demo').owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
    paginationSpeed : 400,
      singleItem : true,
      autoHeight: true,
      navigationText: ['<span class="glyphicon glyphicon-chevron-left"></span>','<span class="glyphicon glyphicon-chevron-right"></span>'],
      afterMove: function() {
          $(window).scrollTop(0);
          $('#androidhinweis').css('display','none');
          $('#ioshinweis').css('display','none');
          $('body').css('padding-top','5%');
      }
  });
    

});
